<?php
require 'parse.php';

$filename = 'chanlist-sample.ini';

if (file_exists('chanlist.ini')) {
	$filename = 'chanlist.ini';
}

$sections = parseChanList($filename, $errors);

if ($sections === false) {
	echo 'error';
	exit;
}
?>
<html>
	<head>
		<meta charset="utf-8">
		<title>IPTV Channel list</title>
	</head>
	<body>
<?php
if (count($sections) > 0) {
	foreach ($sections as $sectionName => $section) {
		$list = $section['_list'];
		
		if (count($list) === 0) {
			continue;
		}
		
		$firstChan = reset($list);
		$headers = array_diff(array_keys($firstChan), array('ChanLogoUrl', 'ChanURL', 'LogoSize', 'LogoLocation', 'LogoDisplay', 'Checkcode', 'TP_frequency', 'DVB_Symborate', 'DVB_Polarity', 'DVB_transportStreamId', 'DVB_Original_network_id', 'DVB_serviceID', 'LogoURL'));
?>
		<h1><?php echo htmlspecialchars($sectionName); ?></h1>
		<table border="1">
<?php
?>
			<thead>
				<tr>
<?php
		foreach ($headers as $header) {
?>
					<th><?php echo htmlspecialchars($header); ?></th>
<?php
		}
?>
				<tr>
			</thead>
			<tbody>
<?php
		foreach ($list as $row) {
?>
				<tr>
<?php
			foreach ($headers as $header) {
?>
					<td><?php echo htmlspecialchars($row[$header]); ?></td>
<?php
			}
?>
				</tr>
<?php
		}
?>
			</tbody>
			<tfoot>
				<tr>
<?php
		foreach ($headers as $header) {
?>
					<th><?php echo htmlspecialchars($header); ?></th>
<?php
		}
?>
				<tr>
			</tfoot>
		</table>
<?php
	}
}

if (count($errors) > 0) {
?>
		<h1>Errors</h1>
<?php
	foreach ($errors as $error) {
		echo '<p>', $error, '</p>', PHP_EOL;
	}
}
?>
	</body>
</html>
