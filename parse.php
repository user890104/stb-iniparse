<?php
function parseChanList($filename, &$errors = array()) {
	$lines = file($filename);

	if ($lines === false) {
		return false;
	}

	$sections = array();
	$errors = array();

	$complete = false;
	$lineNum = 0;
	$currSection = null;
	$currId = 0;

	foreach($lines as $line) {
		++$lineNum;
		
		$line = rtrim($line);
		$len = strlen($line);
		
		if ($len === 0) {
			continue;
		}
		
		if ($line[0] === '[' && $line[$len - 1] === ']') {
			$currId = 0;
			$currSection = substr($line, 1, -1);

			if ($currSection === 'END') {
				$complete = true;
				break;
			}
			
			$sections[$currSection] = array(
				'_list' => array(),
			);
			
			continue;
		}
		
		$data = explode('=', $line, 2);
		
		if (count($data) !== 2) {
			$errors[] = 'Parse error on line ' . $lineNum;
			continue;
		}
		
		$key = &$data[0];
		$value = &$data[1];
		
		switch ($key) {
			case 'ChannelNum':
			case 'ChanID':
			case 'ChanStatus':
			case 'PreviewEnable':
			case 'ChanKey':
			case 'PLTVEnable':
			case 'PauseLength':
			case 'ChanType':
			case 'IsHDChannel':
			case 'ChanEncrypt':
			case 'hasPip':
			case 'pipMulticastPort':
			case 'ChanBandwith':
			case 'FCCEnable':
			case 'PreviewLength':
			case 'PreviewCount':
			case 'HDCPEnable':
			case 'CGMSAEnable':
			case 'MacrovisionEnable':
			case 'FECEnable':
			case 'ChanCacheTime':
				if (is_numeric($value)) {
					$value = intval($value);
				}
			break;
			case 'ChanURL':
				$value = explode('|', $value);
			break;
			case 'LogoSize':
			case 'LogoLocation':
			case 'LogoDisplay':
				$value = explode(',', $value);
			break;
		}
		
		if ($value === 'null') {
			$value = null;
		}
		
		switch ($key) {
			case 'ChannelNum':
			case 'ChannelVer':
				$sections[$currSection][$key] = $value;
			break;
			case 'ChanID':
				$currId = $value;
				$sections[$currSection]['_list'][$currId] = array();
			break;
			default:
				$sections[$currSection]['_list'][$currId][$key] = $value;
			break;
		}
	}

	if (!$complete) {
		$errors[] = 'WARNING: Incomplete file!';
	}

	foreach ($sections as $sectionName => $section) {
		$count = count($section['_list']);
		
		if ($section['ChannelNum'] !== $count) {
			$errors[] = 'WARNING: Wrong channel count for section ' . $sectionName . ' (expected ' . $section['ChannelNum'] . ', got ' . $count . ')';
		}
	}
	
	return $sections;
}
